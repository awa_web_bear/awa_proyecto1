Version 1.0 Tablon de Anuncios

Propuesta basica de solucion, en la cual au falta controlar detalles como:
-Periodo de tiempo en la que es visible una publicacion. 
-Limite de 5 publicaciones por usuario

Proyecto basado en el tutorial de MongoDB, Express y NodeJS. 

Para ejecutar este proyecto(Todo desde la terminal):
-Ingrese a la ruta en donde descargo el repositorio
-Verifique que tiene instalado el docker y docker compose (comando: docker --version o docker-compose --version)
-Ejecute la siguiente linea de comando: docker -compose build
-Una vez terminado odos los procesos ejecutar el comando: docker -compose up
-Despues de que se haya terminado de prepar todo lo necesario, ingresar mediante un navegador al: localHost:5000
